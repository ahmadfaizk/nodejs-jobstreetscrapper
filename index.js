const axios = require('axios')
const cheerio = require('cheerio')
const fs = require('fs')
const chalk = require('chalk')

const url = 'https://www.jobstreet.co.id/id/job-search/job-vacancy.php'
const regex_url = /https:[/][/]w{3}.\w+.\w+.\w+[/]id[/]job[/][\w-]+([/]\w+[/]\w+)?/g
const regex_date = /\d{2}-\w+-\d{4}/g
const bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "October", "Nopember", "Desember"]
const outputFile = 'data.json'
const parsedResults = []
const parsedUrls = []
const pageLimit =  5
let pageCounter = 0

console.log(chalk.yellow.bgBlue(`\nScraping of ${chalk.underline.bold(url)} initiated...\n`))

const getWebsiteUrls = async(url) => {
    try {
        const response = await axios.get(url)
        const $ = cheerio.load(response.data)

        $('#job_listing_panel .panel').map((i, el) => {
            const title = $(el).find('h2').text().trim()
            let url = $(el).find('a.position-title-link').attr('href')
            if (!(title == "" || url == "https://www.jobstreet.co.id/en/job/1")) {
                url = url.match(regex_url).toString()
                parsedUrls.push(url)
            }
        })

        const nextPageLink = $('#pagination_panel').find('#page_next').attr('href')
        //console.log(chalk.cyan(`Scraping: ${nextPageLink}`))
        pageCounter++
        //console.log(`Scraping ${pageCounter/pageLimit*100}%`)
        if (pageCounter === pageLimit) {
            //exportResults(parsedResults)
            return false
        }

        await getWebsiteUrls(nextPageLink)
    } catch (error) {
        //exportResults(parsedResults)
        console.log(error)
    }
}

const exportResults = (parsedResults) => {
    fs.writeFile(outputFile, JSON.stringify(parsedResults, null, 4), (err) => {
        if (err) {
            console.log(err)
        }
        console.log(chalk.yellow.bgBlue(`\n${chalk.underline.bold(parsedResults.length)} Results exported succesfully to ${chalk.underline.bold(outputFile)}\n`))
    })
}

const getJobsData = async(url) => {
    try {
        const response = await axios.get(url)
        const $ = cheerio.load(response.data)
        const position = $('body').find('h1.job-position').text().trim()
        const company = $('body').find('.company_name').text().trim()
        const logo = $('body').find('img#company_logo').attr('data-original')
        const location = $('body').find('span.single_work_location').text().trim()
        const industry = $('body').find('p#company_industry').text().trim()
        const posting_date = getDate($('body').find('p#posting_date').text().trim())
        const closing_date = getDate($('body').find('p#closing_date').text().trim())
        let des = $('body').find('div#job_description').text().trim()
        let description = des.split(/[\n]+[\t]?/g).filter(desc => desc.length > 1)
        return {
            position: position,
            salary: "Lihat di Jobstreet",
            company: company,
            logo: logo,
            location: location,
            industry: industry,
            posting_date: posting_date,
            closing_date: closing_date,
            url: url,
            description: description
        }
    }
    catch(error) {
        console.log(error)
    }
}

const getDate = (date) => {
    const temp = date.match(regex_date).toString().split("-")
    temp[0] = parseInt(temp[0])
    temp[1] = bulan.indexOf(temp[1]) + 1
    temp[2] = parseInt(temp[2])
    return {
      date: temp[0],
      month: temp[1],
      year: temp[2]
    }
}

const getWebsiteContent = async(url) => {
	try {
		await getWebsiteUrls(url)
	    console.log(`${parsedUrls.length} link jobs ditemukan`)
	    parsedUrls.forEach(async (urls) => {
	        const data = await getJobsData(urls)
	        await parsedResults.push(data)
	        console.log(`Scraping ${parsedResults.length/parsedUrls.length*100}% : ${data.position}`)
	        if (parsedResults.length == parsedUrls.length) {
	            exportResults(parsedResults)
	        }
	    })
	}
	catch (error) {
		console.log(error)
	}
}

getWebsiteContent(url)